$(function() {

	$(document).foundation();

	$('textarea.autosize').autosize({
		append: '',
		placeholder: false
	});

	$('img.lazy').unveil(200, function() {
		$(this).load(function() {
			$(this).addClass('loaded');
		});
	});

	$(document).scroll(function() {
		$('#background').css('background-position-y', Math.min(50 + $(this).scrollTop() * .02, 100) + '%');
	}).scroll();

});
