<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Override poster binding to find it by slug.
 */
Route::bind('poster', function($value, $route)
{
    return Poster::whereSlug($value)->firstOrFail();
});

/**
 * Create a new poster.
 */
Route::get('/create', array('as' => 'create', function()
{
	return View::make('create');
}));

/**
 * Store a new poster.
 */
Route::post('/create', array('as' => 'store', function()
{
	$data = Input::all();
	$validator = Poster::validate($data);

	if ($validator->passes())
	{
		$poster = Poster::create(array(
			'message' => $data['message'],
			'background_image' => Input::file('background_image')
		));

		return Redirect::route('show', array('slug' => $poster->slug));
	}

	return Redirect::route('create')->withInput()->withErrors($validator);
}));

/**
 * Browse posters.
 */
Route::get('/browse', array('as' => 'browse', function()
{
	$posters = Poster::orderBy('created_at', 'desc')->remember(10);

	if (Input::has('q'))
		$posters->where('message', 'like', '%' . Input::get('q') . '%');

	$posters = $posters->paginate(48);

	return View::make('browse', compact('posters'));
}));

/**
 * 
 */
Route::post('/preview', array('as' => 'background_preview', function()
{
	$img = Image::cache(function($image)
		{
			// Background color.
			$backgroundColor = Input::get('background_color') ?: '#d00000';

			// Background image.
			$backgroundImage = Input::file('background_image');

			// Create image.
			$image->canvas(Poster::REAL_SIZE, Poster::REAL_SIZE, $backgroundColor);

			// Insert background image if exists.
			if ($backgroundImage)
			{
				try
				{
					$bg = Image::cache(function($image) use($backgroundImage)
						{
							$image->make($backgroundImage->getRealPath());
							$image->fit(Poster::REAL_SIZE);
						},
						30 * 24 * 60
					);

					$image->insert($bg, 'center');
					$image->fill(array(0, 0, 0, .5));
				}
				catch (Exception $e) { }
			}

			// Encode preview image in JPEG.
			$image->encode('jpg');
		},
		30 * 24 * 60,
		true
	);

	// Send dataURI image.
	return $img->response('data-url');
}));

/**
 * 
 */
Route::get('/{poster}/background.{ext}', array('as' => 'background_image', function(Poster $poster, $ext)
{
	return $poster->background(Input::get('s'), $ext)->response();
}));

/**
 * Render poster static image.
 *
 * @param $poster Poster slug.
 * @param $ext    Image extension.
 */
Route::get('/{poster}.{ext}', array('as' => 'render', function(Poster $poster, $ext)
{
	return $poster->render(Input::get('s'), $ext)->response();
}));

/**
 * Show poster.
 *
 * @param $poster Poster slug.
 */
Route::get('/{poster}', array('as' => 'show', function(Poster $poster)
{
	$poster->increment('views');

	return View::make('show', compact('poster'));
}));

/**
 * Homepage.
 */
Route::any('/', function()
{
	// Get distinct posters by background color and image to avoid sames.
	$poster = Poster::random()
		->groupBy('background_color', 'background_image')
		->remember(60)
		->first();

	return View::make('home', compact('poster'));
});
