<?php

/**
 * A poster.
 */
class Poster extends Eloquent {

	/**
	 * Real image generated size.
	 */
	const REAL_SIZE = 1280;

	/**
	 * Fillable fields.
	 */
	protected $fillable = ['message', 'background_image'];

	/**
	 * Virtual fields.
	 */
	protected $appends = ['formated_message', 'sanitized_message', 'title'];

	/**
	 * Model boot method.
	 */
	public static function boot()
	{
		parent::boot();

		// Listen creating event.
		Poster::creating(function($poster)
		{
			// Generate slug.
			$poster->slug = substr(base64_encode(sha1(uniqid(mt_rand(), true))), 0, 8);

			// Check if background image is an instance of UploadedFile.
			if ($poster->background_image instanceof Symfony\Component\HttpFoundation\File\UploadedFile)
			{
				// Check if background image upload is valid.
				if (!$poster->background_image->isValid())
					return false;

				try
				{
					$destination = sprintf('files/uploads/%s/', $poster->slug);
					$filename = sprintf('background.%s', $poster->background_image->getClientOriginalExtension());

					// Move background image into destination folder.
					$poster->background_image->move($destination, $filename);

					// Set background image path.
					$poster->background_image = $destination . $filename;
				}
				catch (FileException $e)
				{
					// Remove background image.
					$poster->background_image = null;
				}
			}
		});
	}

	/**
	 * Return poster validator.
	 *
	 * @param $data Data to validate.
	 * @return Validator Validator instance.
	 */
	public static function validate($data) {
		return Validator::make($data, array(
			'message' => 'required|max:45',
			'background_image' => 'image'
		));
	}

	/**
	 * 
	 *
	 * @param $query 
	 * @return Poster 
	 */
	public function scopeRandom($query) {
		return $query->orderBy(DB::raw('RAND()'));
	}

	/**
	 * Set poster message.
	 *
	 * @param $value Poster message.
	 */
	public function setMessageAttribute($value) {
		$this->attributes['message'] = strip_tags(trim($value));
	}

	/**
	 * Get HTML formated message.
	 *
	 * @return string Formated message.
	 */
	public function getFormatedMessageAttribute() {
		return nl2br(strtoupper($this->message));
	}

	/**
	 * Get sanitized message.
	 *
	 * @return string Sanitized message.
	 */
	public function getSanitizedMessageAttribute() {
		return preg_replace('#\n+#', ' ', $this->message);
	}

	/**
	 * Get title.
	 *
	 * @return string Poster title.
	 */
	public function getTitleAttribute() {
		return "Keep calm and " . $this->sanitized_message;
	}

	/**
	 * Get render image url.
	 *
	 * @return string Render image url.
	 */
	public function getRenderImageAttribute() {
		return URL::route('render', array('slug' => $this->slug, 'ext' => 'png'));
	}

	/**
	 * 
	 *
	 * @return 
	 */
	public function background($size = null, $ext = 'png')
	{
		return Image::cache(function($image) use($size, $ext)
			{
				// Desired size by user.
				$desiredSize = intval($size ?: static::REAL_SIZE);

				// Background color.
				$backgroundColor = $this->background_color ?: '#d00000';

				// Background image.
				$backgroundImage = $this->background_image;

				// Create image.
				$image->canvas(static::REAL_SIZE, static::REAL_SIZE, $backgroundColor);

				// Insert background image if exists.
				if ($this->background_image)
				{
					try
					{
						$bg = Image::cache(function($image) use($backgroundImage)
							{
								$image->make($backgroundImage);
								$image->fit(static::REAL_SIZE);
							},
							30 * 24 * 60
						);

						$image->insert($bg, 'center');
						$image->fill(array(0, 0, 0, .5));
					}
					catch (Exception $e) { }
				}

				// Resize image.
				$image->fit($desiredSize);

				// Encode image to desired format.
				$image->encode($ext, 100);
			},
			30 * 24 * 60,
			true
		);
	}

	/**
	 * Render poster.
	 *
	 * @param $ext Image extension.
	 * @return 
	 */
	public function render($size = null, $ext = 'png')
	{
		return Image::cache(function($image) use($size, $ext)
			{
				// Message lines.
				$lines = explode("\n", $this->message);

				// Desired size by user.
				$desiredSize = intval($size ?: static::REAL_SIZE);

				// Font sizes declaration.
				$fontSizes = array(
					92,                  // Big title (KEEP, ...)
					36,                  // Small title (AND)
					131                  // Crown
				);

				// Margins declaration.
				$margins = array(
					60,                  // Margin top
					$fontSizes[2] + 32,  // Logo image
					$fontSizes[0] + 20,  // KEEP
					$fontSizes[0] + 24,  // CALM (also used by message)
					$fontSizes[1] + 30,  // AND
					60                   // Margin bottom
				);

				// Calculate content height.
				$contentHeight = array_sum($margins) + ($margins[3] * count($lines) - 24);

				// Create image.
				$image->make($this->background()->getCore());

				// Centerize content.
				$y = (static::REAL_SIZE - $contentHeight) / 2 + $margins[0];

				// Text draw function declaration.
				$drawText = function($text, $y, $size) use($image)
				{
					$image->text($text, static::REAL_SIZE / 2, $y, function($font) use($size)
					{
						$font->file('fonts/KeepCalm-Medium.ttf');
						$font->size($size);
						$font->color('#ffffff');
						$font->align('center');
						$font->valign('top');
					});
				};

				// Draw text.
				$drawText('^', $y, $fontSizes[2]);
				$y += $margins[1];

				$drawText('KEEP', $y, $fontSizes[0]);
				$y += $margins[2];

				$drawText('CALM', $y, $fontSizes[0]);
				$y += $margins[3];

				$drawText('AND', $y, $fontSizes[1]);
				$y += $margins[4];

				foreach ($lines as $line)
				{
					$line = strtoupper(trim($line));

					$drawText($line, $y, $fontSizes[0]);
					$y += $margins[2];
				}

				// Resize image.
				$image->fit($desiredSize);

				// Encode image to desired format.
				$image->encode($ext, 100);
			},
			30 * 24 * 60,
			true
		);
	}

}