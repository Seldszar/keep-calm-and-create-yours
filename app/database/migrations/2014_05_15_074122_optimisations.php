<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Optimisations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `posters` CHANGE `slug` `slug` VARCHAR(8) BINARY');
		DB::statement("ALTER TABLE `posters` CHANGE `views` `views` BIGINT UNSIGNED NOT NULL DEFAULT '0'");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
