<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Background extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('posters', function(Blueprint $table)
		{
			$table->string('background_color')->nullable();
			$table->string('background_image')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('posters', function(Blueprint $table)
		{
			$table->dropColumn('background_color');
			$table->dropColumn('background_image');
		});
	}

}