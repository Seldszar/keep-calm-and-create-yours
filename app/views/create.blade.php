@extends('layouts.master')

@section('content')
	<div id="poster-preview">
		<div class="row">
			<div class="column">
				{{ Form::open(['route' => 'store', 'id' => 'create-form', 'files' => true, 'data-abide']) }}
					<nav class="top-bar" data-topbar data-options="is_hover: false">
						<ul class="title-area">
							<li class="name"></li>
							<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
						</ul>
						<section class="top-bar-section">
							<ul class="left">
								<li>
									<a href="#" class="file-button">
										<i class="fa fa-fw fa-picture-o"></i> Background image
										{{ Form::file('background_image', ['id' => 'background-image']) }}
									</a>
								</li>
							</ul>
							<ul class="right">
								<li><button type="submit"><i class="fa fa-fw fa-check"></i> Create</button></li>
						</section>
					</nav>
					<div class="poster-content">
						<section>
							<h1 class="crown">^</h1>
						</section>
						<section>
							<h1>KEEP<br>CALM</h1>
						</section>
						<section>
							<h2>AND</h2>
						</section>
						<section>
							{{
								Form::textarea('message', null, [
									'rows' => 2,
									'maxlength' => 45,
									'autofocus',
									'class' => 'autosize',
									'wrap' => 'hard',
									'placeholder' => 'create
										yours'
								])
							}}
						</section>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script>
		$(function() {
			$('#background-image').change(function() {
				$.ajax('{{ route('background_preview') }}', {
					type: 'POST',
					data: new FormData($('#create-form')[0]),
					contentType: false,
					processData: false,
				}).done(function(data) {
					$('#background').css('background-image', 'url(' + data + ')');
				});
			});
			$('body').css('background-color', '#d00000');
		});
	</script>
@stop
