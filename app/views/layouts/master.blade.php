<!DOCTYPE html>
<html lang="en">
	<head>
		@section('title')
			<title>Keep calm and create yours</title>
		@show
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<meta name="twitter:site" content="@0xSeldszar">
		<meta name="twitter:domain" content="Seldszar.fr">
		<meta name="og:url" content="{{ Request::url() }}">
		@section('card')
			<meta name="twitter:card" content="summary_large_image">
			<meta name="og:title" content="Keep calm and create yours">
			<meta name="og:description" content="Build your own Keep calm poster easily!">
			<meta name="og:image" content="{{ asset('images/summary-large-image.png') }}">
		@show
		<link rel="stylesheet" href="{{ asset('stylesheets/app.css') }}">
		<script src="{{ asset('components/modernizr/modernizr.js') }}"></script>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		@yield('head')
	</head>
	<body>
		<div id="background"></div>
		<div id="wrapper">
			<div id="wrapper-inner">
				<header role="banner">
					<div id="main-nav" class="contain-to-grid">
						<nav class="top-bar" data-topbar data-options="is_hover: false">
							<ul class="title-area">
								<li class="name">
									<h1><a href="{{ asset('/') }}" title="Keep calm and create yours!">KcCy<small>&beta;</small></a></h1>
								</li>
								<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
							</ul>
							<section class="top-bar-section">
								<ul class="right">
									<li class="active"><a href="{{ route('create') }}"><i class="fa fa-fw fa-upload"></i> Create your poster</a></li>
									<li><a href="{{ route('browse') }}"><i class="fa fa-fw fa-folder"></i> Browse gallery</a></li>
								</ul>
							</section>
						</nav>
					</div>
				</header>
				<main role="main">
					@yield('content')
				</main>
				<footer role="contentinfo">
					<div class="row">
						<div class="column">
							<div class="left">
								MADE WITH <i class="fa fa-heart-o"></i> BY <a href="http://seldszar.fr" title="Who's this guy?">SELDSZAR.FR</a>
							</div>
							<div class="button-bar right">
								<!--
								<ul class="button-group">
									<li><a href="mailto:contact@{{ Request::server('SERVER_NAME') }}" class="button secondary" title="I need your feedback"><i class="fa fa-comment"></i></a></li>
								</ul>
								-->
								<ul class="button-group">
									<li><a href="https://twitter.com/0xSeldszar" class="button secondary" title="Follow me on Twitter"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" class="button secondary" title="Buy me a cup of coffee"><i class="fa fa-coffee"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>
		<script src="{{ asset('components/jquery/dist/jquery.min.js') }}"></script>
		<script src="{{ asset('components/foundation/js/foundation.min.js') }}"></script>
		<script src="{{ asset('components/autosize/jquery.autosize.min.js') }}"></script>
		<script src="{{ asset('components/unveil/jquery.unveil.min.js') }}"></script>
		<script src="{{ asset('javascripts/app.js') }}"></script>
		@yield('scripts')
	</body>
</html>
