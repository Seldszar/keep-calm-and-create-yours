@extends('layouts.master')

@section('head')
	<style>
		@if ($poster)
			#background {
				background-image: url('{{ route('background_image', ['poster' => $poster->slug, 'ext' => 'jpg', 's' => 1280]) }}');
			}
		@endif
		#wrapper-inner {
			vertical-align: middle;
		}
	</style>
@stop

@section('content')
	<div class="poster-content">
		<section>
			<h1 class="crown">^</h1>
		</section>
		<section>
			<h1>KEEP<br>CALM</h1>
		</section>
		<section>
			<h2>AND</h2>
		</section>
		<section>
			<h1>CREATE<br>YOURS</h1>
		</section>
	</div>
@stop
