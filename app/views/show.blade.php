@extends('layouts.master')

@section('card')
	<meta name="twitter:card" content="photo">
	<meta name="og:image" content="{{ route('render', ['poster' => $poster->slug, 'ext' => 'jpg', 's' => 640]) }}">
@stop

@section('title')
	<title>{{ $poster->title }}</title>
@stop

@section('head')
	<style>
		#background {
			background-image: url('{{ route('background_image', ['poster' => $poster->slug, 'ext' => 'jpg', 's' => 1280]) }}');
		}
		#wrapper-inner {
			vertical-align: middle;
		}
	</style>
@stop

@section('content')
	<div class="poster-content">
		<section>
			<h1 class="crown">^</h1>
		</section>
		<section>
			<h1>KEEP<br>CALM</h1>
		</section>
		<section>
			<h2>AND</h2>
		</section>
		<section>
			<h1>{{ $poster->formated_message }}</h1>
		</section>
	</div>
@stop
