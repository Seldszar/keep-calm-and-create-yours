@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="column">
			<header>
				{{ Form::open(['method' => 'get', 'id' => 'search-form']) }}
					<div class="row">
						<div class="column small-4 medium-7">
							<label class="inline"><b>{{ $posters->getTotal() }}</b> posters found</label>
						</div>
						<div class="column small-8 medium-5">
							<div class="row collapse">
								<div class="column small-10">
									{{ Form::text('q', Input::get('q'), ['placeholder' => 'Search posters']) }}
								</div>
								<div class="column small-2">
									<button type="submit" class="button secondary postfix"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</header>
			<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4 posters">
				@foreach ($posters as $poster)
					<li>
						<a href="{{ route('show', $poster->slug) }}">
							<img data-src="{{ route('render', array('slug' => $poster->slug, 'ext' => 'jpg', 's' => 364)) }}" alt="Keep calm and {{ $poster->message }}" class="lazy">
						</a>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
@stop

@section('scripts')
	<script>
		$(function() {
			var $th = $('.posters a');
			
			$th.hover(function() {
				$th.not(this).addClass('inactive');
			}, function() {
				$th.removeClass('inactive');
			});
		});
	</script>
@stop
