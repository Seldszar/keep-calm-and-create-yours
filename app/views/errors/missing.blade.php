@extends('layouts.master')

@section('title')
	<title>Keep calm and call Houston</title>
@stop

@section('head')
	<style>
		body {
			background-image: url('{{ asset('images/noise.gif') }}');
		}
		#wrapper-inner {
			vertical-align: middle;
		}
	</style>
@stop

@section('content')
	<div class="poster-content">
		<section>
			<h1 class="crown">^</h1>
		</section>
		<section>
			<h1>KEEP<br>CALM</h1>
		</section>
		<section>
			<h2>AND</h2>
		</section>
		<section>
			<h1>CALL<br>HOUSTON</h1>
		</section>
	</div>
@stop
